//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Import customer model 
const customerModel = require("../model/customerModel");

//function create customer
const createCustomer = (req, res) => {
    //B1: chuẩn bị dữ liệu
    const body = req.body;
    //B2: validate dữ liệu
    if (!body.fullName) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Full name không hợp lệ"
        })
    }
    if (!body.phone || validatePhone(body.phone) === false) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Số điện thoại không hợp lệ"
        })
    }
    if (!body.email || validateEmail(body.email) === false) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Email không hợp lệ"
        })
    }
    //B3: gọi model tạo dữ liệu customer mới
    const newCustomer = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country
    }

    customerModel.create(newCustomer, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(201).json({
            status: "Created new customer successfully",
            data: data
        })
    })
}

//function get all customer
const getAllCustomer = (req, res) => {
    customerModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(201).json({
            status: "get all customers successfully",
            data: data
        })
    })
}

//function get customer by id
const getCustomerById = (req, res) => {
    //B1: chuẩn bị dữ liệu
    const customerId = req.params.customerId;
    //B2: validate id có tồn tại hay không?
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "customer id không hợp lệ"
        })
    }
    //B3: gọi model customer tìm dữ liệu bằng id
    customerModel.findById(customerId, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: "Get customer detail successfully",
            data: data
        })
    })
}

//function update customer by id
const updateCustomerById = (req, res) => {
    //B1: chuẩn bị dữ liệu gồm id và body json
    const customerId = req.params.customerId;
    const body = req.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "customer id không hợp lệ"
        })
    }
    if (!body.fullName) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Full name không hợp lệ"
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Số điện thoại không hợp lệ"
        })
    }
    if (!body.email) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Email không hợp lệ"
        })
    }
    //B3: gọi model update dữ liệu customer
    const updateCustomer = {};
    if (body.fullName !== undefined) {
        updateCustomer.fullName = body.fullName
    }
    if (body.phone !== undefined || validatePhone(body.phone) === false) {
        updateCustomer.phone = body.phone
    }
    if (body.email !== undefined || validateEmail(body.email) === false) {
        updateCustomer.email = body.email
    }
    if (body.address !== undefined) {
        updateCustomer.address = body.address
    }
    if (body.city !== undefined) {
        updateCustomer.city = body.city
    }
    if (body.country !== undefined) {
        updateCustomer.country = body.country
    }
    customerModel.findByIdAndUpdate(customerId, updateCustomer, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: "Update customer successfully",
            data: data
        })
    })
}
//function delete customer by id
const deleteCustomerById = (req, res) => {
    //B1: chuẩn bị dữ liệu
    const customerId = req.params.customerId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "customer id không hợp lệ"
        })
    }
    //B3: gọi model customer bằng id cần xóa
    customerModel.findByIdAndRemove(customerId, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                mesage: error.mesage
            })
        }
        return res.status(200).json({
            status: "Deleted customer successfully"
        })
    })
}
const validatePhone = (number) => {
    "use strict";
    var vform = /(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})\b/;
    if (vform.test(number)) {
        return true;
    }
    else {
        return false;
    }
}
const validateEmail = (paramEmail) => {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    //var address = document.getElementById[email].value;
    if (reg.test(paramEmail) == false) {
        return (false);
    }
}
module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomerById,
    deleteCustomerById
}