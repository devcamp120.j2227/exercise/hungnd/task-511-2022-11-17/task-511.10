//import thư viện mongoose
const mongoose = require("mongoose");

//import order model
const orderModel = require("../model/orderModel");
//import customer model 
const customerModel = require("../model/customerModel");

//function create new order
const createOrder = (req, res) =>{
    //B1: chuẩn bị dữ liệu
    const customerId = req.params.customerId;
    const body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            status: "Bad Request",
            message: "User Id không hợp lệ"
        })
    }
    //B3: thao tác với Database
    const newOrder = {
        _id: mongoose.Types.ObjectId(),
        orderDate: body.orderDate,
        shipperDate: body.shipperDate,
        note: body.note,
        cost:  body.cost
    }
    orderModel.create(newOrder,(error, data) => {
        if(error) {
            return res.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        //thêm Id của order mới vào mảng Order của customer được liên kết bởi ref Order
        customerModel.findByIdAndUpdate(customerId,{
            $push: {
                orders: data._id
            }
        }, (err, updateCustomer) =>{
            if(err){
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return res.status(201).json({
                status: "Create Order Successfully",
                data: data
            })
        })
    })
}
//function get all order
const getAllOrder = (req, res) => {
    orderModel.find((error,data) => {
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status:"Get all orders successfully",
            data: data
        })
    })
}
//function get all order of customer
const getAllOrderOfCustomer = (req, res) => {
    //B1: chuẩn bị dữ liệu
    const customerId = req.params.customerId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            status: "Bad Request",
            message: "Customer Id không hợp lệ"
        })
    }
    //B3: xử lý với database
    customerModel.findById(customerId)
        .populate("orders")
        .exec((error, data) =>{
            if(error){
                return res.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return res.status(200).json({
                status:"Get orders of customer successfully",
                data: data
            })
        })
}
//function get order by id
const getOrderById = (req, res) =>{
    //B1: chuẩn bị dữ liệu
    const orderId = req.params.orderId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return res.status(400).json({
            status: "Bad request",
            message: "Order Id không hợp lệ"
        })
    }
    //B3: gọi model order chứa thông tin
    orderModel.findById(orderId, (error, data) =>{
        if(error){
            return res.status(500).json({
                status:"Internal server error ",
                message: error.message
            })
        }
        return res.status(200).json({
            status:"Get Order detail successfully",
            data: data
        })
    })
}
//function delete order by id
const deleteOrderById = (req, res) => {
    //B1: chuẩn bị dữ liệu
    const orderId = req.params.orderId;
    const customerId = req.params.customerId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return response.status(400).json({
            status:"Bad request",
            message:"Order Id không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            status:"Bad request",
            message:"Customer Id không hợp lệ"
        })
    }
    //B3: gọi model chưa id order để xóa 
    orderModel.findByIdAndDelete(orderId,(error, data) => {
        if(error){
            return res.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        } 
        //Sau khi xóa xong 1 order thì kéo order đó ra khỏi order của customer
    customerModel.findByIdAndUpdate(customerId,{
        $pull:{orders: orderId} 
    },
        (err,updateCustomer) =>{
            if(err){
                return res.status(500).json({
                    status:"Internal server error",
                    message: err.message
                })
            }
            return ReadableStreamBYOBReader.status(200).json({
                status: "Delete Order successfully"
            })
        })
    })
}
module.exports = {
    createOrder,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    deleteOrderById
}