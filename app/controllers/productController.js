//Import thư viện mongoose
const mongoose = require ("mongoose");

//Import Product model
const productModel = require("../model/productModel");

//function create new product
const createProduct = (req, res) => {
    //B1: chuẩn bị dữ liệu
    const body = req.body;

    //B2: validate dữ liệu
    if(!body.name){
        return res.status(400).json({
            status:"Bad Request",
            message: "Tên Product không hợp lệ"
        })
    }
    if(!body.type){
        return res.status(400).json({
            status:"Bad Request",
            message: "Type of Product không hợp lệ"
        })
    }
    if(!body.imageUrl){
        return res.status(400).json({
            status:"Bad Request",
            message: "Image Url không hợp lệ"
        })
    }
    if(isNaN(body.buyPrice) || body.buyPrice < 0){
        return res.status(400).json({
            status:"Bad Request",
            message: "Giá mua không hợp lệ"
        })
    }
    if(isNaN(body.promotionPrice) || body.promotionPrice < 0){
        return res.status(400).json({
            status:"Bad Request",
            message: "Giá khuyến mãi không hợp lệ"
        })
    }

    //B3: gọi model tạo dữ liệu mới
    const newProduct = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    }
    productModel.create(newProduct,(error, data) =>{
        if(error){
            return res.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return res.status(201).json({
            status:"Created new product successfully",
            data: data
        })
    })
}
//function get all product 
const getAllProduct = (req, res) => {
    productModel.find((error, data) =>{
        if(error){
            return res.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status:"Get all Products successfully",
            data: data
        })
    })
}
//function get product by id
const getProductById = (req, res) => {
    //B1: chuẩn bị dữ liệu
    const productId = req.params.productId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)){
        return res.status(400).json({
            status: "Bad request",
            message: "product Id không hợp lệ"
        })
    }
    //B3: gọi model tìm dữ liệu
    productModel.findById(productId, (error, data) =>{
        if(error){
            return res.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status:" Get Product detail successfully",
            data: data
        })
    })
}
//function update product by id
const updateProductById = (req, res) => {
    //B1: chuẩn bị dữ liệu
    const productId = req.params.productId;
    const body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)){
        return res.status(400).json({
            status: "Bad request",
            message: "product Id không hợp lệ"
        })
    }
    if(!body.name){
        return res.status(400).json({
            status:"Bad Request",
            message: "Tên Product không hợp lệ"
        })
    }
    if(!body.type){
        return res.status(400).json({
            status:"Bad Request",
            message: "Type of Product không hợp lệ"
        })
    }
    if(!body.imageUrl){
        return res.status(400).json({
            status:"Bad Request",
            message: "Image Url không hợp lệ"
        })
    }
    if(isNaN(body.buyPrice) || body.buyPrice < 0){
        return res.status(400).json({
            status:"Bad Request",
            message: "Giá mua không hợp lệ"
        })
    }
    if(isNaN(body.promotionPrice) || body.promotionPrice < 0){
        return res.status(400).json({
            status:"Bad Request",
            message: "Giá khuyến mãi không hợp lệ"
        })
    }
    //B3: gọi model update dữ liệu
    const updateProduct = {};
    if(body.name !== undefined){
        updateProduct.name = body.name
    }
    if(body.description !== undefined){
        updateProduct.description = body.description
    }
    if(body.type !== undefined){
        updateProduct.type = body.type
    }
    if(body.imageUrl !== undefined){
        updateProduct.imageUrl = body.imageUrl
    }
    if(body.buyPrice !== undefined){
        updateProduct.buyPrice = body.buyPrice
    }
    if(body.promotionPrice !== undefined){
        updateProduct.promotionPrice = body.promotionPrice
    }
    if(body.amount !== undefined){
        updateProduct.amount = body.amount
    }

    productModel.findByIdAndUpdate(productId, updateProduct, (error, data) =>{
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: "Update product successfully",
            data: data
        })
    })

}
//function delete product  by id
const deleteProductById =  (req, res) => {
    //B1: chuẩn bị dữ liệu
    const productId = req.params.productId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)){
        return res.status(400).json({
            status: "Bad request",
            message: "product Id không hợp lệ"
        })
    }
    //B3: gọi model xóa dữ liệu 
    productModel.findByIdAndDelete(productId,(error, data) => {
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: "Deleted product successfullly"
        })
    })
}
module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProductById,
    deleteProductById
}