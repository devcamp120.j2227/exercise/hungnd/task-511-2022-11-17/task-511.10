//Import thư viện mongoose
const mongoose = require ("mongoose");
const { update } = require("../model/productTypeModel");

//Import ProductType model
const productTypeModel = require("../model/productTypeModel");

//function create product type
const createProductType = (req, res) =>{
    //B1: chuẩn bị dữ liệu
    const body = req.body;
    console.log(body);
    //B2: validate dữ liệu
    if(!body.name){
        return res.status(400).json({
            status: "Bad Request",
            message: "Product Type không hợp lệ"
        })
    }
    //B3: gọi productType model tạo dữ liệu
    const newProductType = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    }
    productTypeModel.create(newProductType,(error, data) =>{
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(201).json({
            status:"Created new product type successfully",
            data: data
        })
    })
}

//function get all product type
const getAllProductType = (req, res) =>{
    //B1: chuẩn bị dữ liệu
    //B2: validate dữ liệu
    //B3: gọi model tạo dữ liệu
    productTypeModel.find((error, data) =>{
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status:"Get all Product Type successfully",
            data: data
        })
    })
}

//function get product type by id
const getProductTypeById =(req, res) =>{
    //B1: chuẩn bị dữ liệu
    const productTypeId = req.params.productTypeId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)){
        return res.status(400).json({
            status: "Bad request",
            message: "product type Id không hợp lệ"
        })
    }
    //B3: gọi model tạo dữ liệu
    productTypeModel.findById(productTypeId,(error, data) =>{
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status:"Get product Type detail successfully",
            data: data
        })
    })
}

//function update product type by id
const updateProductTypeById = (req, res) => {
    //B1: chuẩn bị dữ liệu
    const productTypeId = req.params.productTypeId;
    const body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)){
        return res.status(400).json({
            status: "Bad request",
            message: "product type Id không hợp lệ"
        })
    }
    if(!body.name){
        return res.status(400).json({
            status: "Bad Request",
            message: "Product Type không hợp lệ"
        })
    }
    //B3: gọi model tạo dữ liệu
    const updateProductType = {};

    if(body.name !== undefined){
        updateProductType.name = body.name
    }
    if(body.description !== undefined){
        updateProductType.description = body.description
    }

    productTypeModel.findByIdAndUpdate(productTypeId, updateProductType, (error, data) =>{
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status:"Update product type successfully",
            data: data
        })
    })
}
//function delete product type by id
const deleteProductTypeById =  (req, res) => {
    //B1: chuẩn bị dữ liệu
    const productTypeId = req.params.productTypeId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)){
        return res.status(400).json({
            status: "Bad request",
            message: "product type Id không hợp lệ"
        })
    }
    //B3: gọi model xóa dữ liệu 
    productTypeModel.findByIdAndDelete(productTypeId,(error, data) => {
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: "Deleted product type successfullly"
        })
    })
}
module.exports ={
    createProductType,
    getAllProductType,
    getProductTypeById,
    updateProductTypeById,
    deleteProductTypeById
}